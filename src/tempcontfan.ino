#include <DHT.h>

const unsigned DHT_PIN = 2; // The pin the DH22 sensor is connected to for input
#define DHTTYPE DHT22 // DHT 22  (AM2302)
DHT dht(DHT_PIN, DHTTYPE);  // Initialize DHT sensor for normal 16mhz Arduino
const unsigned PWM_PIN = 3;

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop()
{
    delay(2000);
    const float hum = dht.readHumidity(); // Humidity
    const float temp = dht.readTemperature(); // Temperature

    Serial.print("Humidity: ");
    Serial.print(hum);
    Serial.print(" %, Temp: ");
    Serial.print(temp);
    Serial.println(" Celsius");
    unsigned fan_speed_pwm;
    const int temp_round = static_cast<int>(temp);
    
    if (temp_round < 26) { 
      fan_speed_pwm = 0;
    } else if (temp_round == 26) {
      fan_speed_pwm = 51;
    } else if (temp_round == 27) {
      fan_speed_pwm = 102;
    } else if (temp_round == 28) {
      fan_speed_pwm = 153;
    } else if (temp_round == 29) {
      fan_speed_pwm = 204;
    } else if (temp_round > 29) {
      fan_speed_pwm = 255;
    }
    
    analogWrite(PWM_PIN, fan_speed_pwm);
    Serial.print("Fan speed: ");
    const float fan_speed_percent = (fan_speed_pwm / 255) * 100;
    Serial.print( (fan_speed_percent ? String(fan_speed_percent) + "%" : "Off") );
    delay(100);
    
    delay(3000);
}